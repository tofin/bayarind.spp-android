package com.mustofin.bayarindspp.application

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import androidx.navigation.Navigation
import androidx.navigation.ui.NavigationUI
import androidx.navigation.ui.setupActionBarWithNavController
import com.mustofin.bayarindspp.R
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        setContentView(R.layout.activity_main)

        setupToolbar()
        setupNavigation()
    }

    override fun onSupportNavigateUp(): Boolean {
        return Navigation.findNavController(this, R.id.fragmentContainer).navigateUp()
    }

    private fun setupToolbar() {
        setSupportActionBar(toolbarMain)
    }

    private fun setupNavigation() {
//        bottomNavigationViewMain.itemIconTintList = null
        val navController = Navigation.findNavController(this, R.id.fragmentContainer)
        setupActionBarWithNavController(navController)
        NavigationUI.setupWithNavController(bottomNavigationViewMain, navController)
    }

    fun hideNavigation(value: Boolean) {
        if (value) {
            bottomNavigationViewMain.visibility = View.GONE
        } else {
            bottomNavigationViewMain.visibility = View.VISIBLE
        }
    }


    fun hideToolbar(value: Boolean) {
        if (value) {
            toolbarMain.visibility = View.GONE
        } else {
            toolbarMain.visibility = View.VISIBLE
        }
    }
}
