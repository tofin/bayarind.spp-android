package com.mustofin.bayarindspp.application.menu.home

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.mustofin.bayarindspp.R
import com.mustofin.bayarindspp.application.MainActivity

class HomeFragment : Fragment() {


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onStart() {
        super.onStart()
        (activity as MainActivity).hideToolbar(true)
    }

    override fun onStop() {
        super.onStop()
        (activity as MainActivity).hideToolbar(false)
    }
}
